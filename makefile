CC=gcc
CFLAGS=-I.
DEPS = linenoise.h header.h
OBJ = OS.o driver.o linenoise.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

shellmake: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)