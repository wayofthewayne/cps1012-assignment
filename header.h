#ifndef OSASSINGMENT_HEADER_H
#define OSASSINGMENT_HEADER_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

#define MAXVAR 15
#define MAX_ARGS 225
#define MAXNAME 50
#define STANDARRSIZE 50
#define CUSTOM_EXIT_CODE 4

// Define type sig_t for signal function prototypes
typedef void (*sig_t)(int signum);

//Function prototypes
void EnvCreate();
int Parser(void);
bool Echo(char **args, int startPoint);
bool VarChange(char *assingmentStatement);
char *DollarHandle(char **args, int startPoint, int charCount);
bool ChangeDirectory(char *args);
bool Unset(char *Var);
void ShowEnv(bool redirectArrow, bool appendArrow, char *filename);
void Source(char *filename);
bool CommandRecognition(char **args);
bool fork_and_exec(char *const *args, char *filename, bool redirectArrow, bool appendArrow);
void generic_handler(int signum);
void install_handler(int signum, sig_t handler);
bool WriteToFile(char **args, int startPoint, int checkForRedir, char *mode);
bool WriteExternalFile(char *filename, bool redirectArrow, bool appendArrow);
long GetFileSize(FILE *f);
char *ReadFileToString(char *fileName, char *mode);

#endif //OSASSINGMENT_HEADER_H
